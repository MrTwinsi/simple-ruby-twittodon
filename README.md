simple-ruby-twittodon
-----------------------------------------------
Steps for installation

**1 install ruby and rails :**
    install rails 4.2.7.1
    install ruby 2.3.3p222 (2016-11-21) [x86_64-linux-gnu]

**2 get the files :**
    
    for gitlab:
         clone the project master branch at https://gitlab.com/MrTwinsi/simple-ruby-twittodon
    for zip file :
         extract the file in one of your folders
    
**3 install the components :**
    open terminal in the root file of the project (cd simple-ruby-twittodon)
    excecute command "bundle install --path vendor/bundle"
    
**4 initialise the database :**
    in your project folder, run the command "rake db:migrate"
    and then "rake db:setup"
    
**5 run the server :**
    to lunch server : bundle exec rails s
    Connect with a web browser to he adress specified by your terminal