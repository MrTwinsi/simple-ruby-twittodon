class User < ActiveRecord::Base
  has_many :tweets
  validates :name, :img_url, presence: true
end
