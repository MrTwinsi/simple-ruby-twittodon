class Addtweetstouser < ActiveRecord::Migration
  def change
    change_table :tweets do |t|
      t.references :users, foreign_key: true
    end
  end
end
