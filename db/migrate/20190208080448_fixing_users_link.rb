class FixingUsersLink < ActiveRecord::Migration
  def change
      remove_column :tweets, :users_id, :integer
      add_column :tweets, :user_id, :integer
  end
end
