class AddForeignKey < ActiveRecord::Migration
  def change
    remove_column :tweets, :user_id, :integer
    add_reference :tweets,:user, index: true
    add_foreign_key :tweets, :users
  end
end
