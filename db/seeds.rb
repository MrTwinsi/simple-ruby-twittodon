# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

user_1 = User.create(name: "Marcellion",img_url:"https://i.imgflip.com/nhlto.jpg")
user_2 = User.create(name: "Mollanio",img_url:"https://i.imgflip.com/3ru9d.jpg")
user_3 = User.create(name: "BollyBob",img_url:"https://i.kym-cdn.com/entries/icons/original/000/021/971/Salt-Bae-001.jpg")
user_4 = User.create(name: "xXx_-_dakiller-idy-death-69_-_xXx",img_url:"https://media1.tenor.com/images/4f65861080c5bbc159d7d17b2604b23c/tenor.gif")
user_5 = User.create(name: "Aprillo",img_url:"https://i.kym-cdn.com/entries/icons/original/000/025/051/inital.jpg")

tweetUser_1_1 = Tweet.create(content:"mon 1er tweet !", user_id: 1)
tweetUser_1_2 = Tweet.create(content:"comment va le monde ?", user_id: 1)
tweetUser_1_3 = Tweet.create(content:"j'ai manger au moins 10 gauffres", user_id: 1)
tweetUser_1_4 = Tweet.create(content:"le ciel est bien bleu aujourd'hui", user_id: 1)
tweetUser_1_5 = Tweet.create(content:"la mauvaise journée content que cela soit fini !", user_id: 1)
tweetUser_1_6 = Tweet.create(content:"enfin un nouveau jeu pour ma xbox !", user_id: 1)
tweetUser_1_7 = Tweet.create(content:"vous aves des coupons xbox live gold ?", user_id: 1)

tweetUser_2_1 = Tweet.create(content:"c'est mes premiers pas ici", user_id: 2)
tweetUser_2_2 = Tweet.create(content:"comment va tout monde ?", user_id: 2)
tweetUser_2_3 = Tweet.create(content:"j'aime bien le nouveau Avangers", user_id: 2)
tweetUser_2_4 = Tweet.create(content:"nouvelle production marvel bientôt au ciné !", user_id: 2)

tweetUser_3_1 = Tweet.create(content:"un petit pas pour moi !", user_id: 3)
tweetUser_3_2 = Tweet.create(content:"j'adore les oiseaux", user_id: 3)
tweetUser_3_3 = Tweet.create(content:"super la visite de la volière au oiseaux", user_id: 3)
tweetUser_3_4 = Tweet.create(content:"beau temps aujourd'hui", user_id: 3)
tweetUser_3_5 = Tweet.create(content:"un journée géniale", user_id: 3)
tweetUser_3_6 = Tweet.create(content:"J'ai hate de visiter le parc aux oiseaux encore une fois", user_id: 3)

tweetUser_4_1 = Tweet.create(content:"un bon début", user_id: 4)
tweetUser_4_2 = Tweet.create(content:"tweeter c'est bien", user_id: 4)

tweetUser_5_1 = Tweet.create(content:"j'ai enfin commencé !", user_id: 5)
tweetUser_5_2 = Tweet.create(content:"mangez de bon fruits !", user_id: 5)
tweetUser_5_3 = Tweet.create(content:"les sport, ça aide à être en bonne forme", user_id: 5)
tweetUser_5_4 = Tweet.create(content:"petite promenade de 10 km aujourd'hui, c'était ouf !", user_id: 5)
tweetUser_5_5 = Tweet.create(content:"nouvelle paire de chossures pour courrir !", user_id: 5)
tweetUser_5_6 = Tweet.create(content:"grosse pluie aujourd'hui donc natation \"au sec\"", user_id: 5)
tweetUser_5_7 = Tweet.create(content:"quel bodrier prendre pour de l'escalade ?", user_id: 5)
tweetUser_5_8 = Tweet.create(content:"bientôt l'hiver, vive le Ski !", user_id: 5)
tweetUser_5_8 = Tweet.create(content:"petit karting pour apprendre à conduire", user_id: 5)

