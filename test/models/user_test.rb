require 'test_helper'

class UserTest < ActiveSupport::TestCase

  test "basic persistence" do
    user = User.new
    user.name = "Michel"
    user.img_url = "123"
    assert user.save
  end

  test "name presence validation" do
    user = User.new
    assert_not user.save
    assert_equal ["Name can't be blank", "Img url can't be blank"], user.errors.full_messages
  end

  #https://guides.rubyonrails.org/getting_started.html


end
